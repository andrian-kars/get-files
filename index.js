const fs = require('fs');
const path = require('path')
const express = require('express')
const app = express()
app.use(express.json())

const filesPath = './files/'
// create files dir
if (!fs.existsSync(filesPath)) fs.mkdirSync(filesPath)

const allowedFiles = ['log', 'txt', 'json', 'yaml', 'xml', 'js']

let files = null
const getFiles = () => {
    files = []
    return fs.readdirSync(filesPath).forEach(file => {
        files.push(file)
    })
}

app.post('/api/files', (req, res) => {
    const fileName = req.body.filename
    const fileContent = req.body.content

    // errors
    if (!fileName) return res.status(400).json({ message: 'Please specify \'filename\' parameter' })
    if (fs.existsSync(`./files/${fileName}`)) return res.status(400).json({ message: `File with name ${fileName} already exists` })
    if (!fileContent) return res.status(400).json({ message: 'Please specify \'content\' parameter' })
    // getting the extension and check if it if correct one
    const splitted = fileName.split('.')
    if (!allowedFiles.includes(splitted[splitted.length - 1])) res.status(400).json({ message: 'You use wrong file format' })

    fs.writeFileSync(`./files/${fileName}`, fileContent)
    return res.json({ message: 'File created successfully' })
})

app.get('/api/files', (req, res) => {
    getFiles()
    if (files.length === 0) {
        return res.status(400).json({ message: 'Client error' })
    } else {
        return res.json({ message: 'Success', files: files })
    }
})

app.get('/api/files/:paramsFileName', (req, res) => {
    const { paramsFileName } = req.params

    getFiles()
    if (!files.includes(paramsFileName)) return res.status(400).json({ message: `No file with '${paramsFileName}' filename found` })

    const currentFileName = paramsFileName
    const currentContent = fs.readFileSync(`./files/${currentFileName}`, 'utf8')
    const currentExtension = path.extname(`./files/${currentFileName}`)
    const currentDate = fs.statSync(`./files/${currentFileName}`).birthtime

    return res.json({
        message: 'Success',
        filename: currentFileName,
        content: currentContent,
        extension: currentExtension.slice(1, currentExtension.length), // remove . (.txt -> txt)
        uploadedDate: currentDate
    })
})

app.put('/api/files/:paramsFileName', (req, res) => {
    const { paramsFileName } = req.params
    const fileContent = req.body.content

    getFiles()
    if (!files.includes(paramsFileName)) return res.status(400).json({ message: `No file with '${paramsFileName}' filename found` })
    if (!allowedFiles.includes(paramsFileName.split('.')[1])) res.status(400).json({ message: 'You use wrong file format' })
    if (!fileContent) return res.status(400).json({ message: 'Please specify \'content\' parameter' })

    fs.writeFileSync(`./files/${paramsFileName}`, fileContent)
    return res.json({ message: 'File changed successfully' })
})

app.delete('/api/files/:paramsFileName', (req, res) => {
    const { paramsFileName } = req.params

    getFiles()
    if (!files.includes(paramsFileName)) {
        return res.status(400).json({ message: `No file with '${paramsFileName}' filename found` })
    } else {
        fs.unlinkSync(`./files/${paramsFileName}`)
        return res.json({ message: 'File deleted successfully' })
    }
})

// 404
app.use((req, res) => {
    res.status(404).end('User error')
})

app.listen(8080)